import re
from datetime import datetime
from functools import partial

import pytest
from httpx import Client
from httpx import WSGITransport

from drakaina import check_permissions
from drakaina import ENV_AUTH_SCOPES
from drakaina import ENV_USER
from drakaina import ENV_USER_ID
from drakaina import login_required
from drakaina import match_all
from drakaina import match_any
from drakaina import remote_procedure
from drakaina.contrib.jwt.errors import JWTBackendError
from drakaina.contrib.jwt.errors import ValidationJWTTokenError
from drakaina.contrib.jwt.middleware import JWTAuthenticationMiddleware
from drakaina.contrib.jwt.utils import _extra_validation  # noqa
from drakaina.contrib.jwt.utils import _validate_algorithm  # noqa
from drakaina.contrib.jwt.utils import _validate_payload  # noqa
from drakaina.contrib.jwt.utils import copy_payload
from drakaina.contrib.jwt.utils import decode_jwt_token
from drakaina.contrib.jwt.utils import encode_jwt_token
from drakaina.utils import iterable_str_arg
from drakaina.wsgi import WSGIHandler


URL = "/jrpc"
HOST = "http://testserver"
SECRET_STRING = "__dont_store_in_a_repo__"

USER_TABLE = {
    "_1": {"name": "Richard Doe", "groups": ["admin"]},
    "_2": {"name": "John Doe", "groups": []},
    "_3": {"name": "Jane Doe", "groups": "post:create, post:update"},
}

JWT_PAYLOAD = {"sub": "1234567890", "name": "John Doe", "iat": 1516239022}
JWT_TOKEN = (
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
    "eyJuYW1lIjoiSm9obiBEb2UiLCJzdWIiOiIxMjM0NTY3ODkwIiwiaWF0IjoxNTE2MjM5MDIyfQ"
    ".S1cjKQjRD4uJo5N9drPGPvP4gSYzRz8YOTiWusAg7yY"
)
#
TOKEN_PAYLOAD_1 = {
    "iat": 1645387340,
    "exp": 7325582540,
    "user_id": "_1",
    "scp": "admin",
}
VALID_TOKEN_1 = (
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
    "eyJpYXQiOjE2NDUzODczNDAsImV4cCI6NzMyNTU4MjU0MCwidXNlcl9pZCI6Il8xIiwic2Nw"
    "IjoiYWRtaW4ifQ."
    "zUrc0zSZXO_KoqbTF3eAIadFw9SgMo32LYL5SkITY34"
)
TOKEN_PAYLOAD_2 = {
    "iat": 1645387340,
    "exp": 7325582540,
    "user_id": "_2",
    "scp": [],
}
VALID_TOKEN_2 = (
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
    "eyJpYXQiOjE2NDUzODczNDAsImV4cCI6NzMyNTU4MjU0MCwidXNlcl9pZCI6Il8yIiwic2"
    "NwIjpbXX0."
    "GqyKaxO9r3Xo4ad8pmELVGCwHb4LeBQdlyV_ab2ajhA"
)
TOKEN_PAYLOAD_3 = {
    "iat": 1645387340,
    "exp": 7325582540,
    "user_id": "_3",
    "scp": ["post:create", "post:update"],
}
VALID_TOKEN_3 = (
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
    "eyJpYXQiOjE2NDUzODczNDAsImV4cCI6NzMyNTU4MjU0MCwidXNlcl9pZCI6Il8z"
    "Iiwic2NwIjpbInBvc3Q6Y3JlYXRlIiwicG9zdDp1cGRhdGUiXX0."
    "HBJ-J5PNXa9N8gbT8HTSN6ppiXpmKhgtSA9BYogfvqs"
)
INVALID_TOKEN = VALID_TOKEN_1[:-2] + "xx"


@pytest.fixture
def app_factory():
    def factory(**jwt_kw):
        return WSGIHandler(
            route=URL,
            middlewares=[
                partial(
                    JWTAuthenticationMiddleware,
                    secret_key=SECRET_STRING,
                    **jwt_kw,
                ),
            ],
        )

    return factory


@pytest.fixture
def client_factory():
    headers = {"Content-Type": "application/json"}
    return partial(
        Client,
        base_url=HOST,
        headers=headers,
    )


@remote_procedure(name="public_data")
def public_procedure():
    return ["data_x", "data_y", "data_z"]


def test_public_procedure(app_factory, client_factory):
    app = app_factory(credentials_required=False)
    client = client_factory(transport=WSGITransport(app))

    rpc = {"jsonrpc": "2.0", "method": "public_data", "id": 123}
    r = client.post(URL, json=rpc)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # with credentials
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }


def test_public_procedure_and_required_credentials(app_factory, client_factory):
    app = app_factory(credentials_required=True)  # it's default
    client = client_factory(transport=WSGITransport(app))

    # Call public procedure without credentials
    rpc = {"jsonrpc": "2.0", "method": "public_data", "id": 123}
    r = client.post(URL, json=rpc)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {
            "code": -32010,
            "data": "Credential required",
            "message": "Authentication failed",
        },
        "id": None,
    }

    # with credentials
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }


def get_user(request, payload):
    return USER_TABLE[payload[ENV_USER_ID]]


def get_scopes(request, payload):
    return payload.get("scp")


@login_required
@remote_procedure(provide_request=True)
def user_data(request):
    return [request.user, request[ENV_USER]]


@login_required
@remote_procedure(provide_request=True)
def scope_data(request):
    return iterable_str_arg(request[ENV_AUTH_SCOPES])


def test_get_user_and_get_scopes(app_factory, client_factory):
    app = app_factory(user_getter=get_user, scopes_getter=get_scopes)
    client = client_factory(transport=WSGITransport(app))
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}

    rpc = {"jsonrpc": "2.0", "method": "user_data", "id": 123}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": [
            {"name": "Richard Doe", "groups": ["admin"]},
            {"name": "Richard Doe", "groups": ["admin"]},
        ],
        "id": 123,
    }

    rpc = {"jsonrpc": "2.0", "method": "scope_data", "id": 123}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["admin"],
        "id": 123,
    }


@login_required
@remote_procedure(name="private_data")
def private_procedure():
    return ["data_x", "data_y", "data_z"]


def test_private_procedure(app_factory, client_factory):
    app = app_factory()
    client = client_factory(transport=WSGITransport(app))
    rpc = {"jsonrpc": "2.0", "method": "private_data", "id": 123}

    # Private procedure call by user ID _1
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Private procedure call by user ID _2
    headers = {"Authorization": f"Bearer {VALID_TOKEN_2}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Private procedure call by user ID _3
    headers = {"Authorization": f"Bearer {VALID_TOKEN_3}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Private procedure call by unknown user
    headers = {"Authorization": f"Bearer {INVALID_TOKEN}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {
            "code": -32011,
            "message": "Invalid token",
            "data": "Token is invalid or expired",
        },
        "id": None,
    }


@check_permissions(["admin", "post:create"], match_any)
@remote_procedure(name="protected_data")
def protected_procedure():
    return ["data_x", "data_y", "data_z"]


def test_protected_procedure(app_factory, client_factory):
    app = app_factory(user_getter=get_user, scopes_getter=get_scopes)
    client = client_factory(transport=WSGITransport(app))
    rpc = {"jsonrpc": "2.0", "method": "protected_data", "id": 123}

    # Protected procedure call by user ID _1
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Protected procedure call by user ID _2
    headers = {"Authorization": f"Bearer {VALID_TOKEN_2}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {"code": -32012, "message": "Forbidden", "data": "Forbidden"},
        "id": 123,
    }

    # Protected procedure call by user ID _3
    headers = {"Authorization": f"Bearer {VALID_TOKEN_3}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Protected procedure call by unknown user
    headers = {"Authorization": f"Bearer {INVALID_TOKEN}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {
            "code": -32011,
            "message": "Invalid token",
            "data": "Token is invalid or expired",
        },
        "id": None,
    }


@check_permissions("post:update, post:create", match_all)  # Default comparator
@remote_procedure(name="very_protected_data")
def very_protected_procedure():
    return ["data_x", "data_y", "data_z"]


def test_very_protected_procedure(app_factory, client_factory):
    app = app_factory(user_getter=get_user, scopes_getter=get_scopes)
    client = client_factory(transport=WSGITransport(app))
    rpc = {"jsonrpc": "2.0", "method": "very_protected_data", "id": 123}

    # Protected procedure call by user ID _1
    headers = {"Authorization": f"Bearer {VALID_TOKEN_1}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {"code": -32012, "message": "Forbidden", "data": "Forbidden"},
        "id": 123,
    }

    # Protected procedure call by user ID _2
    headers = {"Authorization": f"Bearer {VALID_TOKEN_2}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {"code": -32012, "message": "Forbidden", "data": "Forbidden"},
        "id": 123,
    }

    # Protected procedure call by user ID _3
    headers = {"Authorization": f"Bearer {VALID_TOKEN_3}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "result": ["data_x", "data_y", "data_z"],
        "id": 123,
    }

    # Protected procedure call by unknown user
    headers = {"Authorization": f"Bearer {INVALID_TOKEN}"}
    r = client.post(URL, json=rpc, headers=headers)
    assert r.status_code == 200
    assert r.json() == {
        "jsonrpc": "2.0",
        "error": {
            "code": -32011,
            "message": "Invalid token",
            "data": "Token is invalid or expired",
        },
        "id": None,
    }


# Test utils


def test_decode_jwt_token():
    assert decode_jwt_token(JWT_TOKEN, SECRET_STRING) == JWT_PAYLOAD


def test_encode_jwt_token():
    payload = JWT_PAYLOAD.copy()
    sub, iat = payload.pop("sub"), datetime.fromtimestamp(payload.pop("iat"))

    assert (
        encode_jwt_token(
            SECRET_STRING,
            payload=payload,
            subject=sub,
            issued_at=iat,
        )
        == JWT_TOKEN
    )


@pytest.mark.parametrize(
    "payload",
    (
        {"iss": "string_a"},
        {"sub": "string_b"},
        {"aud": "string_c"},
        {"exp": 2123456789},
        {"nbf": 1123456789},
        {"iat": 1123456789},
        {"jti": "string_d"},
        {"jtt": "string_e"},
        {"scp": "string_f,string_g"},
    ),
)
def test_encoding_decoding(payload):
    p = payload.copy()
    # as datetime
    exp = datetime.fromtimestamp(p.pop("exp")) if "exp" in p else None
    nbf = datetime.fromtimestamp(p.pop("nbf")) if "nbf" in p else None
    token = encode_jwt_token(
        SECRET_STRING,
        payload=p,
        issuer=p.pop("iss", None),
        subject=p.pop("sub", None),
        audience=p.pop("aud", None),
        expiration=exp,
        not_before=nbf,
        issued_at=p.pop("iat", None),  # as timestamp
        token_id=p.pop("jti", None),
        token_type=p.pop("jtt", None),
        permission_scopes=p.pop("scp", None),
    )

    decoded_payload = decode_jwt_token(
        token,
        SECRET_STRING,
        verify_values=payload,
    )
    assert decoded_payload == payload


@pytest.mark.parametrize(
    ("payload", "no_copy", "result"),
    (
        (
            {"user_id": "zero"},
            None,
            ({"user_id": "zero"}, {}),
        ),
        (
            {"exp": 1, "iat": 0},
            ("exp", "iat"),
            ({}, {}),
        ),
        (
            {"sub": "subject_name", "aud": "audience_name"},
            ("exp", "iat"),
            ({}, {"subject": "subject_name", "audience": "audience_name"}),
        ),
        (
            {
                "user_id": "zero",
                "exp": 1,
                "iat": 0,
                "sub": "subject_name",
                "aud": "audience_name",
            },
            ("exp", "iat"),
            (
                {"user_id": "zero"},
                {"subject": "subject_name", "audience": "audience_name"},
            ),
        ),
    ),
    ids=(
        "user data only",
        "only filtered data",
        "only allowed jwt data",
        "mixed data",
    ),
)
def test_copy_payload(payload, no_copy, result):
    args = ()
    if no_copy:
        args = (no_copy,)

    assert result == copy_payload(payload, *args)


@pytest.mark.parametrize(
    ("decoded_token", "verify_values"),
    (
        (
            {
                "payload": {
                    "claim_r": "value",
                    "claim": "value",
                    "claim_l": "value",
                },
                "header": {},
            },
            {
                "claim": "value",
                "claim_l": ("value", "other"),
                "claim_r": re.compile("^value?"),
            },
        ),
        (
            {
                "header": {
                    "hdr_r": "value",
                    "hdr": "value",
                    "hdr_l": "value",
                },
                "payload": {},
            },
            {
                "header": {
                    "hdr": "value",
                    "hdr_l": ("value", "other"),
                    "hdr_r": re.compile("^value?"),
                },
            },
        ),
    ),
    ids=("payload", "headers"),
)
def test_extra_validation(decoded_token, verify_values):
    _extra_validation(decoded_token, verify_values)


@pytest.mark.parametrize(
    ("decoded_token", "verify_values"),
    (
        (
            {
                "payload": {
                    "claim_r": "fail",
                    "claim": "fail",
                    "claim_l": "fail",
                },
                "header": {},
            },
            {
                "claim": "value",
                "claim_l": ("value", "other"),
                "claim_r": re.compile("^value?"),
            },
        ),
        (
            {
                "header": {
                    "hdr_r": "fail",
                    "hdr": "fail",
                    "hdr_l": "fail",
                },
                "payload": {},
            },
            {
                "header": {
                    "hdr": "value",
                    "hdr_l": ("value", "other"),
                    "hdr_r": re.compile("^value?"),
                },
            },
        ),
    ),
    ids=("payload", "headers"),
)
def test_extra_validation_errors(decoded_token, verify_values):
    with pytest.raises(ValidationJWTTokenError):
        _extra_validation(decoded_token, verify_values)


@pytest.mark.parametrize(
    "algorithm",
    ("HS256", "HS384", "HS512"),
)
def test_validate_algorithm(algorithm):
    _validate_algorithm(algorithm)


def test_validate_algorithm_errors():
    with pytest.raises(JWTBackendError):
        _validate_algorithm("unknown")

    for algorithm in ("RS256", "RS384", "RS512"):
        with pytest.raises(ModuleNotFoundError):
            _validate_algorithm(algorithm)


@pytest.mark.parametrize(
    "payload",
    (
        {"user_id": "zero"},
        {"my_note": "not_my"},
    ),
)
def test_validate_payload(payload):
    _validate_payload(payload)


@pytest.mark.parametrize(
    "payload",
    (
        {"iss": "issuer"},
        {"sub": "subject"},
        {"jtt": "token_type"},
        {"scp": "scope"},
    ),
)
def test_validate_payload_errors(payload):
    with pytest.raises(JWTBackendError):
        _validate_payload(payload)
