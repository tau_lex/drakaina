from functools import partial

import pytest
from httpx import Client
from httpx import WSGITransport

from drakaina import remote_procedure
from drakaina.middleware.cors import CORSMiddleware
from drakaina.serializers import JsonSerializer
from drakaina.wsgi import WSGIHandler


json = JsonSerializer()
URL = "/jrpc"
HOST = "http://testserver"
ANOTHER_HOST = "http://anotherdomain"
PAYLOAD = json.serialize({"jsonrpc": "2.0", "method": "get_text", "id": 0})
EXPECTED = json.serialize({"jsonrpc": "2.0", "result": "Some Text", "id": 0})
X_HEADER = "X-Header"


@pytest.fixture
def client_factory():
    headers = {"Content-Type": "application/json"}
    return partial(
        Client,
        base_url=HOST,
        headers=headers,
    )


@remote_procedure
def get_text():
    return "Some Text"


app = WSGIHandler(route=URL, openrpc_url=URL)


def test_allow_all(client_factory):
    cors_app = CORSMiddleware(
        app,
        allow_origin=["*"],
        allow_headers=["*"],
        allow_methods=["*"],
        expose_headers=[X_HEADER],
        allow_credentials=True,
    )
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": HOST,
        "Access-Control-Request-Method": "GET",
        "Access-Control-Request-Headers": "X-Example",
    }
    r = client.options(URL, headers=headers)
    assert r.status_code == 200
    assert r.text == ""
    assert r.headers["access-control-allow-origin"] == HOST
    assert r.headers["access-control-allow-headers"] == "X-Example"
    assert r.headers["access-control-allow-credentials"] == "true"
    assert r.headers["vary"] == "Origin"

    # Standard response
    r = client.post(URL, headers={"Origin": HOST}, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert r.headers["access-control-allow-origin"] == "*"
    assert r.headers["access-control-allow-credentials"] == "true"
    assert r.headers["access-control-expose-headers"] == X_HEADER

    # Standard credentialed response
    headers = {"Origin": HOST, "Cookie": "star_cookie=sugar"}
    r = client.post(URL, headers=headers, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert r.headers["access-control-allow-origin"] == HOST
    assert r.headers["access-control-expose-headers"] == X_HEADER
    assert r.headers["access-control-allow-credentials"] == "true"

    # Non-CORS response
    r = client.post(URL, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert "access-control-allow-origin" not in r.headers


def test_allow_all_except_credentials(client_factory):
    cors_app = CORSMiddleware(
        app,
        allow_origin=["*"],
        allow_headers=["*"],
        allow_methods=["*"],
        expose_headers=[X_HEADER],
    )
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": HOST,
        "Access-Control-Request-Method": "POST",
        "Access-Control-Request-Headers": "X-Example",
    }
    r = client.options(URL, headers=headers)
    assert r.status_code == 200
    assert r.text == ""
    assert r.headers["access-control-allow-origin"] == "*"
    assert r.headers["access-control-allow-headers"] == "X-Example"
    assert "access-control-allow-credentials" not in r.headers
    assert "vary" not in r.headers

    # Standard response
    r = client.post(URL, headers={"Origin": HOST}, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert r.headers["access-control-allow-origin"] == "*"
    assert r.headers["access-control-expose-headers"] == X_HEADER
    assert "access-control-allow-credentials" not in r.headers

    # Non-CORS response
    r = client.post(URL, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert "access-control-allow-origin" not in r.headers


def test_allow_specific_origin(client_factory):
    cors_app = WSGIHandler(
        route=URL,
        middlewares=[
            partial(
                CORSMiddleware,
                allow_origin=[HOST, "https://example.com"],
                allow_headers=["X-Example", "Content-Type"],
            ),
        ],
    )
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": HOST,
        "Access-Control-Request-Method": "GET",
        "Access-Control-Request-Headers": "X-Example, Content-Type",
    }
    r = client.options(URL, headers=headers)
    assert r.status_code == 200
    assert r.text == ""
    assert r.headers["access-control-allow-origin"] == HOST
    assert r.headers["access-control-allow-headers"] == (
        "Accept, Accept-Encoding, Accept-Language, Authorization, "
        "Content-Language, Content-Type, DNT, Origin, User-Agent, "
        "X-Example, X-Requested-With"
    )
    assert "access-control-allow-credentials" not in r.headers

    # Standard response
    r = client.post(URL, headers={"Origin": HOST}, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert r.headers["access-control-allow-origin"] == HOST
    assert "access-control-allow-credentials" not in r.headers

    # Non-CORS response
    r = client.post(URL, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert "access-control-allow-origin" not in r.headers


def test_disallowed_preflight(client_factory):
    cors_app = CORSMiddleware(
        app,
        allow_origin=[HOST],
        allow_headers=[X_HEADER],
    )
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": ANOTHER_HOST,
        "Access-Control-Request-Method": "POST",
        "Access-Control-Request-Headers": "X-Another",
    }
    r = client.options(URL, headers=headers)
    assert r.status_code == 200
    assert r.text == ""
    assert "access-control-allow-origin" not in r.headers


def test_allowed_preflight_request_if_origins_wildcard_and_credentials_allowed(
    client_factory,
):
    cors_app = CORSMiddleware(
        app,
        allow_origin=["*"],
        allow_methods=["POST"],
        allow_credentials=True,
    )
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": HOST,
        "Access-Control-Request-Method": "POST",
    }
    r = client.options(URL, headers=headers)
    assert r.status_code == 200
    assert r.headers["access-control-allow-origin"] == HOST
    assert r.headers["access-control-allow-credentials"] == "true"
    assert r.headers["vary"] == "Origin"


def test_allow_available_methods(client_factory):
    cors_app = CORSMiddleware(app, allow_origin=["*"], allow_methods=["*"])
    client = client_factory(transport=WSGITransport(cors_app))

    # Pre-flight response
    headers = {
        "Origin": HOST,
        "Access-Control-Request-Method": "POST",
    }
    for method in ("GET", "HEAD", "OPTIONS", "POST", "DELETE"):
        r = client.options(URL, headers=headers)
        assert r.status_code == 200
        assert method in r.headers["access-control-allow-methods"]

    # Standard response
    r = client.get(URL, headers={"Origin": HOST})
    assert r.status_code == 200
    r = client.post(URL, headers={"Origin": HOST}, content=PAYLOAD)
    assert r.status_code == 200


def test_credentialed_requests_return_specific_origin(client_factory):
    cors_app = CORSMiddleware(app, allow_origin=["*"])
    client = client_factory(transport=WSGITransport(cors_app))

    # Credentialed request
    headers = {"Origin": HOST, "Cookie": "star_cookie=sugar"}
    r = client.post(URL, headers=headers, content=PAYLOAD)
    assert r.status_code == 200
    assert r.content == EXPECTED
    assert r.headers["access-control-allow-origin"] == HOST
    assert "access-control-allow-credentials" not in r.headers


def test_vary_header_defaults_to_origin(client_factory):
    cors_app = CORSMiddleware(app, allow_origin=[HOST])
    client = client_factory(transport=WSGITransport(cors_app))

    r = client.get(URL, headers={"Origin": HOST})
    assert r.status_code == 200
    assert r.headers["vary"] == "Origin"

    r = client.post(URL, headers={"Origin": HOST}, content=PAYLOAD)
    assert r.status_code == 200
    assert r.headers["vary"] == "Origin"


def test_allowed_origin_does_not_leak_between_credentialed_requests(
    client_factory,
):
    cors_app = CORSMiddleware(
        app,
        allow_origin=["*"],
        allow_headers=["*"],
        allow_methods=["*"],
    )

    client = client_factory(transport=WSGITransport(cors_app))
    r = client.get(URL, headers={"Origin": HOST})
    assert r.headers["access-control-allow-origin"] == "*"
    assert "access-control-allow-credentials" not in r.headers

    r = client.get(URL, headers={"Cookie": "foo=bar", "Origin": HOST})
    assert r.headers["access-control-allow-origin"] == HOST
    assert "access-control-allow-credentials" not in r.headers

    r = client.get(URL, headers={"Origin": HOST})
    assert r.headers["access-control-allow-origin"] == "*"
    assert "access-control-allow-credentials" not in r.headers
