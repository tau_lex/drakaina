from drakaina import remote_procedure


@remote_procedure(name="sum")
def rpc_sum(*args: int) -> int:
    return sum(args)


@remote_procedure
def subtract(minuend: int, subtrahend: int) -> int:
    """Subtract method.

    :param minuend: Minuend.
    :param subtrahend: Subtrahend.
    :return: Difference.
    """
    return minuend - subtrahend


@remote_procedure
def update(*args):
    print(f"Notification `update` called. Your params={args}")


@remote_procedure
def notify_hello(*args):
    print(f"Hello! Your params={args}")


@remote_procedure(name="get_data", provide_request=True)
def get_data(request):
    print(f"You called `get_data`. Request object: {request}.")
    return ["hello", 5]
