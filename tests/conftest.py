from typing import NamedTuple
from typing import Union

import pytest


# JSON-RPC 2.0 Examples


@pytest.fixture(scope="session")
def rpc_procedures():
    """The fixture is required to register the test procedures."""
    import tests.rpc_methods  # noqa: F401


class Pair(NamedTuple):
    request: Union[str, dict, list]
    response: Union[str, dict, list]


origin_spec_examples = (
    # rpc call with positional parameters:
    Pair(
        {"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1},
        {"jsonrpc": "2.0", "result": 19, "id": 1},
    ),
    Pair(
        {"jsonrpc": "2.0", "method": "subtract", "params": [23, 42], "id": 2},
        {"jsonrpc": "2.0", "result": -19, "id": 2},
    ),
    # rpc call with named parameters:
    Pair(
        {
            "jsonrpc": "2.0",
            "method": "subtract",
            "params": {"subtrahend": 23, "minuend": 42},
            "id": 3,
        },
        {"jsonrpc": "2.0", "result": 19, "id": 3},
    ),
    Pair(
        {
            "jsonrpc": "2.0",
            "method": "subtract",
            "params": {"minuend": 42, "subtrahend": 23},
            "id": 4,
        },
        {"jsonrpc": "2.0", "result": 19, "id": 4},
    ),
    # a Notification:
    Pair(
        {"jsonrpc": "2.0", "method": "update", "params": [1, 2, 3, 4, 5]},
        "",
    ),
    Pair({"jsonrpc": "2.0", "method": "foobar"}, ""),
    # rpc call of non-existent method:
    Pair(
        {"jsonrpc": "2.0", "method": "foobar", "id": "1"},
        {
            "jsonrpc": "2.0",
            "error": {"code": -32601, "message": "Method not found"},
            "id": "1",
        },
    ),
    # rpc call with invalid JSON:
    Pair(
        '{"jsonrpc": "2.0", "method": "foobar, "params": "bar", "baz]',
        {
            "jsonrpc": "2.0",
            "error": {"code": -32700, "message": "Parse error"},
            "id": None,
        },
    ),
    # rpc call with invalid Request object:
    Pair(
        {"jsonrpc": "2.0", "method": 1, "params": "bar"},
        {
            "jsonrpc": "2.0",
            "error": {"code": -32600, "message": "Invalid Request"},
            "id": None,
        },
    ),
    # rpc call Batch, invalid JSON:
    Pair(
        '[{"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"}, '
        ' {"jsonrpc": "2.0", "method"]',
        {
            "jsonrpc": "2.0",
            "error": {"code": -32700, "message": "Parse error"},
            "id": None,
        },
    ),
    # rpc call with an empty Array:
    Pair(
        [],
        {
            "jsonrpc": "2.0",
            "error": {"code": -32600, "message": "Invalid Request"},
            "id": None,
        },
    ),
    # rpc call with an invalid Batch (but not empty):
    Pair(
        [1],
        [
            {
                "jsonrpc": "2.0",
                "error": {"code": -32600, "message": "Invalid Request"},
                "id": None,
            },
        ],
    ),
    # rpc call with invalid Batch:
    Pair(
        [1, 2, 3],
        [
            {
                "jsonrpc": "2.0",
                "error": {"code": -32600, "message": "Invalid Request"},
                "id": None,
            },
            {
                "jsonrpc": "2.0",
                "error": {"code": -32600, "message": "Invalid Request"},
                "id": None,
            },
            {
                "jsonrpc": "2.0",
                "error": {"code": -32600, "message": "Invalid Request"},
                "id": None,
            },
        ],
    ),
    # rpc call Batch:
    Pair(
        [
            {"jsonrpc": "2.0", "method": "sum", "params": [1, 2, 4], "id": "1"},
            {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]},
            {
                "jsonrpc": "2.0",
                "method": "subtract",
                "params": [42, 23],
                "id": "2",
            },
            {"foo": "boo"},
            {
                "jsonrpc": "2.0",
                "method": "foo.get",
                "params": {"name": "myself"},
                "id": "5",
            },
            {"jsonrpc": "2.0", "method": "get_data", "id": "9"},
        ],
        [
            {"jsonrpc": "2.0", "result": 7, "id": "1"},
            {"jsonrpc": "2.0", "result": 19, "id": "2"},
            {
                "jsonrpc": "2.0",
                "error": {"code": -32600, "message": "Invalid Request"},
                "id": None,
            },
            {
                "jsonrpc": "2.0",
                "error": {"code": -32601, "message": "Method not found"},
                "id": "5",
            },
            {"jsonrpc": "2.0", "result": ["hello", 5], "id": "9"},
        ],
    ),
    # rpc call Batch (all notifications):
    Pair(
        [
            {"jsonrpc": "2.0", "method": "notify_sum", "params": [1, 2, 4]},
            {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]},
        ],
        "",
    ),
)
ids = (
    "rpc call with positional parameters #1",
    "rpc call with positional parameters #2",
    "rpc call with named parameters #1",
    "rpc call with named parameters #2",
    "a Notification #1",
    "a Notification #2",
    "rpc call of non-existent method",
    "rpc call with invalid JSON",
    "rpc call with invalid Request object",
    "rpc call Batch, invalid JSON",
    "rpc call with an empty Array",
    "rpc call with an invalid Batch (but not empty)",
    "rpc call with invalid Batch",
    "rpc call Batch",
    "rpc call Batch (all notifications)",
)


@pytest.fixture(params=origin_spec_examples, ids=ids)
def jrpc_spec_example(request):
    """Returns one of origin examples pair of request/response"""
    return request.param
