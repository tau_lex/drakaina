# Environ (Request) context
ENV_APP = "drakaina.app"
ENV_IS_AUTHENTICATED = "auth.is_authenticated"
ENV_AUTH_EXCEPTION = "auth.exception"
ENV_USER = "user"
ENV_USER_ID = "user_id"
ENV_AUTH_PAYLOAD = "auth.payload"
ENV_AUTH_SCOPES = "auth.scopes"

# RPC procedure fields
RPC_REGISTRY = "__rpc_registry"
RPC_NAME = "__rpc_name"
RPC_REGISTERED = "__rpc_procedure"
RPC_PROVIDE_REQUEST = "__rpc_provide_request"
RPC_SCHEMA = "__rpc_schema"
RPC_META = "__rpc_metadata"
